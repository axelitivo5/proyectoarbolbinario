class Nodo:
    def __init__(self, valor):
        self.valor = valor
        self.izquierda = None
        self.derecha = None


class ArbolBinario:
    def __init__(self):
        self.raiz = None

    def insertar(self, elemento):
        if self.raiz is None:
            self.raiz = Nodo(elemento)
        else:
            self._insertar_recursivo(self.raiz, elemento)

    def _insertar_recursivo(self, nodo, elemento):
        if elemento < nodo.valor:
            if nodo.izquierda is None:
                nodo.izquierda = Nodo(elemento)
            else:
                self._insertar_recursivo(nodo.izquierda, elemento)
        else:
            if nodo.derecha is None:
                nodo.derecha = Nodo(elemento)
            else:
                self._insertar_recursivo(nodo.derecha, elemento)


    def existe(self, elemento):
        if self.raiz is None:
            return None
        nodo_actual = self.raiz
        while nodo_actual.derecha is not None:
            nodo_actual = nodo_actual.derecha
        return nodo_actual.valor

    def maximo(self):
        if self.raiz is None:
            return None
        

        nodo_actual = self.raiz
        while nodo_actual.derecha is not None:
            nodo_actual = nodo_actual.derecha
        return nodo_actual.valor

    def minimo(self):
        if self.raiz is None:
            return None
        

        nodo_actual = self.raiz
        while nodo_actual.izquierda is not None:
            nodo_actual = nodo_actual.izquierda
        return nodo_actual.valor



    def altura(self):
        return self._altura_recursiva(self.raiz)

    def _altura_recursiva(self, nodo):
        if nodo is None:
            return -1
        altura_izquierda = self._altura_recursiva(nodo.izquierda)
        altura_derecha = self._altura_recursiva(nodo.derecha)
        return 1 + max(altura_izquierda, altura_derecha)

    
    def obtener_elementos(self):
        elementos = []
        self._obtener_elementos_inorder(self.raiz, elementos)
        return elementos

    def _obtener_elementos_inorder(self, nodo, elementos):
        if nodo:
            self._obtener_elementos_inorder(nodo.izquierda, elementos)
            elementos.append(nodo.valor)
            self._obtener_elementos_inorder(nodo.derecha, elementos)

    def __str__(self):
        pass
